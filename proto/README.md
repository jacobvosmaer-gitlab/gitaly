# Vendored copy of gitaly-proto

Vendored from gitlab.com/gitlab-org/gitaly-proto at 617898b9c0c4c8a55c7b0abe95144109a3836270.

Migration in progress, see
https://gitlab.com/gitlab-org/gitaly/issues/1761. Do not edit files in
this directory, your changes will be ignored and overwritten.
